import os, sys
from ping import do_one as ping_one

import os, sys, random, time
from PyQt5.QtWidgets import QApplication, QMainWindow, QTableView, QFileDialog, QDialog, QMessageBox
from PyQt5.QtGui import QPixmap, QStandardItemModel, QStandardItem, QTextCursor
from PyQt5.QtCore import QRunnable, pyqtSlot, pyqtSignal, QThreadPool, QObject, Qt, QVariant, QSortFilterProxyModel

from PyQt5 import uic
uic.compileUiDir('.', False)
from ui_mainwindow import Ui_MainWindow
from ui_about_dialog import Ui_AboutDialog

class PingWorkerSignals(QObject):
    finished = pyqtSignal()
    abort = pyqtSignal()

    # str:  message, int: index
    log = pyqtSignal(str, int)

    # float: ping value, int: number in range
    result = pyqtSignal(float, int)

    # int: index
    progress = pyqtSignal(int)

class PingWorker(QRunnable):
    def __init__(self, prefix, low, high):
        super(PingWorker, self).__init__()
        self.setAutoDelete(True)

        self.abort = False

        self.signals = PingWorkerSignals()
        self.signals.abort.connect(self.set_abort)

        self.prefix = prefix
        self.low = low
        self.high = high

    def set_abort(self):
        self.abort = True

    def do_ping(self, dest, count=4, timeout=1):
        lats = []
        for i in range(count):
            lat = ping_one(dest, timeout=timeout)
            if lat is not None:
                lats.append(lat)
        if len(lats) > 1:
            return sum(lats) / len(lats)
        if len(lats) == 1:
            return lats[0]
        return None

    @pyqtSlot()
    def run(self):
        print("Worker: %d -> %d" % (self.low, self.high))
        for i in range(self.low, self.high):
            time.sleep(0.05)
            if self.abort is True: break
            self.signals.progress.emit(i)
            hostname = "%s%d.nordvpn.com" % (self.prefix, i)
            latency = None
            try:
                latency = self.do_ping(hostname, count=4)
            except Exception as ex:
                self.signals.log.emit(str(ex), i)
            if latency is None: continue
            self.signals.result.emit(latency, i)
        self.signals.finished.emit()

class AboutDialog(QDialog, Ui_AboutDialog):
    def __init__(self, parent):
        super(AboutDialog, self).__init__(parent)
        self.setupUi(self)
        self.setModal(True)

        #with open('LICENSE.txt') as f:
        #    self.txtLicenses.setPlainText(f.read())

class MainWindow(QMainWindow, Ui_MainWindow):
    class NumberSortModel(QSortFilterProxyModel):
        def lessThan(self, left, right):
            lval = left.data()
            rval = right.data()
            return lval < rval

    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)

        self.worker = None
        self.running = False
        self.pool = QThreadPool.globalInstance()
        self.model = QStandardItemModel(1, 2, self)
        self.model.setHorizontalHeaderLabels(["Hostname", "Latency (ms)"])

        sortproxy = MainWindow.NumberSortModel()
        sortproxy.setSourceModel(self.model)
        self.listServers.setModel(sortproxy)
        self.listServers.setSortingEnabled(True)
        self.listServers.sortByColumn(1, Qt.AscendingOrder)

        self.numLow.setValue(322) # seems reasonable
        self.progressBar.hide()

        self.cmdStart.clicked.connect(self.start_stop)
        self.actionExport.triggered.connect(self.action_export)
        self.actionAbout.triggered.connect(self.action_about)

    @pyqtSlot()
    def action_about(self):
        print('about dialog')
        dlg = AboutDialog(self)
        dlg.show()

    def closeEvent(self, event):
        if self.worker is not None: self.worker.signals.abort.emit()
        event.accept()

    @pyqtSlot()
    def action_export(self):
        if self.model.rowCount() <= 0 or self.model.item(0) is None:
            QMessageBox.warning(self, "Ooops", "You don't have anything to export.")
            return


        filepath, _ = QFileDialog.getSaveFileName(self, "Export CSV", ".", "CSV (*.csv)")
        if filepath is None: return
        print("Exporting to", filepath)

        with open(filepath, 'w') as f:
            for i in range(self.model.rowCount()):
                name = self.model.item(i, 0).text()
                latency = self.model.item(i, 1).data(Qt.DisplayRole)
                print(name, latency)
                f.write("%s,%d\n" % (name, latency))
                f.flush()

    @pyqtSlot()
    def start_stop(self):
        if self.running is True:
            print("stop")
            self.running = False
            self.worker.signals.abort.emit()
            self.cmdStart.setEnabled(False)


        else:
            print("start")
            self.running = True
            self.cmdStart.setText("Stop pinging")
            self.model.clear()
            self.model.setHorizontalHeaderLabels(["Hostname", "Latency (ms)"])
            self.txtLog.clear()
            self.progressBar.show()
            self.progressBar.setMinimum(self.numLow.value())
            self.progressBar.setMaximum(self.numHigh.value())

            self.worker = PingWorker(prefix=self.txtRegionPrefix.text(), low=self.numLow.value(), high=self.numHigh.value())
            self.worker.signals.finished.connect(self.worker_done)
            self.worker.signals.result.connect(self.worker_result)
            self.worker.signals.log.connect(self.worker_log)
            self.worker.signals.progress.connect(self.worker_progress)
            self.pool.start(self.worker)

    @pyqtSlot(int)
    def worker_progress(self, idx):
        self.progressBar.setValue(idx)

    @pyqtSlot(str, int)
    def worker_log(self, msg, idx):
        logmsg = "%s%d - %s\n" % (self.txtRegionPrefix.text(), idx, msg)
        self.txtLog.insertPlainText(logmsg)
        self.txtLog.moveCursor(QTextCursor.End)
        self.statusBar().showMessage(logmsg)

    @pyqtSlot(float, int)
    def worker_result(self, latency, idx):
        #item = QStandardItem("%s%d - %.3f ms" % (self.txtRegionPrefix.text(), idx, latency))
        latencyitem = QStandardItem()
        latencyitem.setData(QVariant(int(latency*1000),), Qt.DisplayRole)

        self.model.appendRow([QStandardItem(self.txtRegionPrefix.text() + str(idx)), latencyitem])
        self.statusBar().showMessage("%s%d - %.3f ms" % (self.txtRegionPrefix.text(), idx, latency))

    @pyqtSlot()
    def worker_done(self):
        print('ui reports worker is done')
        self.statusBar().showMessage("PingWorker reports done.")
        self.progressBar.setValue(self.numHigh.value())
        self.running = False
        self.cmdStart.setEnabled(True)
        self.cmdStart.setText("Start pinging")


def main():
    #print(ping_one("us1337.nordvpn.com", timeout=1))
    app = QApplication(sys.argv)
    win = MainWindow()
    win.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
