# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\ui_about_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_AboutDialog(object):
    def setupUi(self, AboutDialog):
        AboutDialog.setObjectName("AboutDialog")
        AboutDialog.resize(580, 282)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(AboutDialog)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(AboutDialog)
        self.groupBox.setTitle("")
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.groupBox)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setObjectName("label")
        self.verticalLayout_4.addWidget(self.label)
        self.txtLicenses = QtWidgets.QPlainTextEdit(self.groupBox)
        self.txtLicenses.setReadOnly(True)
        self.txtLicenses.setBackgroundVisible(False)
        self.txtLicenses.setObjectName("txtLicenses")
        self.verticalLayout_4.addWidget(self.txtLicenses)
        self.verticalLayout.addWidget(self.groupBox)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.buttonBox = QtWidgets.QDialogButtonBox(AboutDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(True)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout_2.addWidget(self.buttonBox)

        self.retranslateUi(AboutDialog)
        self.buttonBox.accepted.connect(AboutDialog.accept)
        self.buttonBox.rejected.connect(AboutDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(AboutDialog)

    def retranslateUi(self, AboutDialog):
        _translate = QtCore.QCoreApplication.translate
        AboutDialog.setWindowTitle(_translate("AboutDialog", "About NordPing"))
        self.label.setText(_translate("AboutDialog", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600;\">NordPing by Sharky - sharky@sharky.pw</span></p></body></html>"))
        self.txtLicenses.setPlainText(_translate("AboutDialog", "This project is licensed under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International\n"
"http://creativecommons.org/licenses/by-nc-sa/4.0/\n"
"\n"
"ping.py:\n"
"    Copyright (c) Matthew Dixon Cowles, <http://www.visi.com/~mdc/>.\n"
"    Distributable under the terms of the GNU General Public License\n"
"    version 2. Provided with no warranties of any sort.\n"
"\n"
"    Original Version from Matthew Dixon Cowles:\n"
"      -> ftp://ftp.visi.com/users/mdc/ping.py\n"
"\n"
"    Rewrite by Jens Diemer:\n"
"      -> http://www.python-forum.de/post-69122.html#69122"))
        self.txtLicenses.setPlaceholderText(_translate("AboutDialog", "Licenses go here!"))

