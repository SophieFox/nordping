from cx_Freeze import setup, Executable

import sys
base = "Win32GUI" if sys.platform == 'win32' else None

executables = [
    Executable('nordping.py', base=base)
]

from PyQt5 import uic
uic.compileUiDir('.', False)

buildOptions = dict(
    packages = [],
    excludes = [],
    includes = [],
    include_files = ['LICENSE.txt']
)

setup(
    name='nordping',
    version='0.1',
    packages=[],
    url='https://sharky.pw',
    license='Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International',
    author='Sharky',
    author_email='sharky@sharky.pw',
    description='PyQT5 GUI utility to find the closest NordVPN server',
    options=dict(build_exe = buildOptions),
    executables=executables,
    install_requires=[
        'PyQt5',
    ],
)
